//
//  DataController.swift
//  GooganInterviewTask
//
//  Created by MACBOOK AIR on 12/31/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit
import CoreData

class DataController: NSObject {
    
    
    //MARK:- Save CoreData
    func save(arrayOfDic: [ItemModel]){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entity =  NSEntityDescription.entity(forEntityName: "Item", in:managedObjectContext)
        
        for dic:ItemModel in arrayOfDic {
            let newItem = Item(entity: entity!, insertInto: managedObjectContext)
            self.initItemCoreData(item: newItem, itemModel: dic)
            
            do{
                try managedObjectContext.save()
            }catch {
                fatalError("Cant save core data")
            }
        }
    }
    
    
    
    //MARK:- Fetch CoreData
    func fetchItems( compeletion: (_ exist: Bool, _ data: [ItemModel]?) ->Void)  {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            compeletion(false, nil)
            return
        }
        
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
        
        do {
            let itemFetch = try managedObjectContext.fetch(fetchRequest) as? [Item]
            if let count: Int = itemFetch?.count, count > 0 {
                compeletion(true, self.itemCoreDataToItemMode(itemCoreData: itemFetch!))
            }else{
                compeletion(false, nil)
            }
        }catch {
            fatalError("Couldnt fetch")
        }
    }
    
    
    //MARK:- Delete CoreData
    func deleteCoredata() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
        
        
        if let result = try? managedObjectContext.fetch(fetchRequest) {
            for object in result {
                managedObjectContext.delete(object as! NSManagedObject)
            }
        }
    }
    
    
    //MARK- 
    
    func itemCoreDataToItemMode(itemCoreData: [Item]) -> [ItemModel]{
        var itemsModelArray:[ItemModel] = []
        
        for item:Item in itemCoreData {
            let itemModel: ItemModel = ItemModel(itemCoreData: item)
            itemsModelArray.append(itemModel)
        }
        
        return itemsModelArray
    }
    
    func initItemCoreData(item: Item, itemModel:ItemModel) {
        item.transferID = itemModel.transferID
        item.fromAddress = itemModel.fromAddress
        item.fromLat = itemModel.fromLat
        item.fromLong = itemModel.fromLong
        item.toAddress = itemModel.toAddress
        item.toLat = itemModel.toLat
        item.toLong = itemModel.toLong
    }

    
    

}
