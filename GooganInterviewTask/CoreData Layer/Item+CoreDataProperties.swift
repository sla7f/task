//
//  Item+CoreDataProperties.swift
//  GooganInterviewTask
//
//  Created by MACBOOK AIR on 12/30/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item");
    }

    @NSManaged public var fromAddress: String?
    @NSManaged public var fromLat: String?
    @NSManaged public var fromLong: String?
    @NSManaged public var toAddress: String?
    @NSManaged public var toLat: String?
    @NSManaged public var toLong: String?
    @NSManaged public var transferID: String?

}
