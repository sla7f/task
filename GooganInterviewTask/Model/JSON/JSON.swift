//
//  JSON.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class JSON {
    
    var message: AnyObject!
    var stauts: String!
    
    init(dic: Dictionary<String,String>){
        self.stauts  = dic["status"] 
        self.message = dic["message"] as AnyObject!
    }
    
    init(withDicAnyObject: Dictionary<String,AnyObject>) {
        self.stauts  = withDicAnyObject["status"] as! String
        self.message = withDicAnyObject["message"] as! [Dictionary<String,String>] as AnyObject!
    }

}
