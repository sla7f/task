//
//  ItemModel.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/28/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class ItemModel: NSObject {
    
    let jsonTransferID: String  = "TransferID"
    let jsonFromAddress: String = "FromAddress"
    let jsonFromLat: String     = "FromLat"
    let jsonFromLong: String    = "FromLong"
    let jsonToAddress: String   = "ToAddress"
    let jsonToLat: String       = "ToLat"
    let jsonToLong: String      = "ToLong"
    
    var transferID: String!
    var fromAddress: String!
    var fromLat: String!
    var fromLong: String!
    var toAddress: String!
    var toLat: String!
    var toLong: String!
    
    init(dic: Dictionary<String,String>) {
        self.transferID = dic[self.jsonTransferID]
        self.fromAddress = dic[self.jsonFromAddress]
        self.fromLat = dic[self.jsonFromLat]
        self.fromLong = dic[self.jsonFromLong]
        self.toAddress = dic[self.jsonToAddress]
        self.toLat = dic[self.jsonToLat]
        self.toLong = dic[self.jsonToLong]
    }
    
    init(itemCoreData: Item) {
        self.transferID = itemCoreData.transferID
        self.fromAddress = itemCoreData.fromAddress
        self.fromLat = itemCoreData.fromLat
        self.fromLong = itemCoreData.fromLong
        self.toAddress = itemCoreData.toAddress
        self.toLat = itemCoreData.toLat
        self.toLong = itemCoreData.toLong
    }
}
