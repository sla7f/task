//
//  LoginAPI.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit
import Alamofire

class LoginAPI: NSObject {
    
    let rootURL: String = "http://qtech-system.com/interview/index.php/apis/login"
    
    let params:[String: String] = ["un": "userName",
                                    "up": "userPassword",
                                    "name": "test",
                                    "password": "123"]
    
    
    func loginAPIByUsernamePassword(_ compeletion: @escaping (_ status: Bool , _ message: AnyObject) ->Void) {
        Alamofire.request(rootURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in

                print(response.result)   // result of response serialization
                
                if let json = response.result.value as? Dictionary<String, AnyObject> {
                    print("JSON \(json)")
                    if let success = json["status"] as? String , success == "success"{
                        let returnJSON: JSON = JSON(withDicAnyObject: json )
                        compeletion(true, returnJSON.message)
                        
                    }else{
                        let returnJSON: JSON = JSON(dic: json as! Dictionary<String, String>)
                        compeletion(false, returnJSON.message as! String as AnyObject)
                    }
                }else {
                    compeletion(false, "Network Error, make sure you connecto to Internet" as AnyObject)
                }
        }
    }


}
