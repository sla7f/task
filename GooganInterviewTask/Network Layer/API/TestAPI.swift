//
//  TestAPI.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit
import Alamofire

class TestAPI: NSObject {
    
    let rootURL: String = "http://qtech-system.com/interview/index.php/apis/testApi"
    
    let params:Dictionary<String,String>!
    
    init(userName: String, password: String) {
        self.params = ["un": userName,
                       "up": password]
    }
    
    func testAPIByUsernamePassword(_ compeletion: @escaping (_ status: String , _ message: String) ->Void) {
        Alamofire.request(rootURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                print(response.result)   // result of response serialization
                
                if let json = response.result.value {
                    let returnJSON: JSON = JSON(dic: json as! Dictionary<String, String>)
                    compeletion(returnJSON.stauts, returnJSON.message as! String)
                }else{
                    compeletion("FAILURE", "Network Error, make sure you connecto to Internet")
            }
        }
    }
    
    
}
