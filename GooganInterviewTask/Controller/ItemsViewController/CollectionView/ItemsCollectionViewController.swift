//
//  ItemsCollectionViewController.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/29/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class ItemsCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    
    var itemsCollectionViewArray: [ItemModel]! {
        didSet{
            self.itemsCollectionView?.reloadData()
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- UICollectionView DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsCollectionViewArray.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ItemsCollectionViewCell = self.itemsCollectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! ItemsCollectionViewCell
        
        // configure Cell
        let itemModel: ItemModel = self.itemsCollectionViewArray[indexPath.row]
        cell.itemFromAddressLabel.text = itemModel.fromAddress
        cell.itemToAddressLabel.text = itemModel.toAddress
        cell.itemImageView.image = UIImage(named: "Place")
        
        cell.itemToAddressLabel.numberOfLines = 0
        cell.itemFromAddressLabel.numberOfLines = 0
        cell.itemFromAddressLabel.adjustsFontSizeToFitWidth = true
        cell.itemToAddressLabel.adjustsFontSizeToFitWidth = true

        
        return cell
    }
    
    //MARK:- UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = collectionViewSize.width/2.0 - 10 //Display Three elements in a row.
        collectionViewSize.height = collectionViewSize.height/5.0
        return collectionViewSize
    }
    
    

}
