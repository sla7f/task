//
//  ItemsViewController.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ItemsViewController: UIViewController {
    
    @IBOutlet weak var tableViewContainerView: UIView!
    @IBOutlet weak var collectionViewContainerView: UIView!
    @IBOutlet weak var listGridBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var itemsArray:[ItemModel]?  {
        didSet{
        }
    }
    
    var doubleTap : Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        itemsArray = []
        
        // init by disable loadingActivityIndicator
        self.loadingActivityIndicator.stopAnimating()
        
        // reachabilty Scenario
        self.reachabiltyScenario()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- FillItemsArray
    
    func fillItemsArray() {
        
        // play loadingActivityIndicator
        self.loadingActivityIndicator.startAnimating()
        
        // first check CoreData count 
        guard let appDeleget = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        appDeleget.dataController.fetchItems { (isSucces: Bool, itemsModelArray:[ItemModel]?) in
            if (isSucces == true){
                // delete all objects to make sure that you save all updates from api
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appDelegate.dataController.deleteCoredata()
                
                // get from API
                self.getfromApi()
                
            }else{
                // get from Api
                self.getfromApi()
            }
        }
    }
    
    
    //MARK:- 
    
    func getfromApi() {
        let loginAPI: LoginAPI = LoginAPI()
        loginAPI.loginAPIByUsernamePassword { (status, message) in
            if status{
                // success
                for item in message as! [Dictionary<String,String>] {
                    let itemModel:ItemModel = ItemModel(dic: item)
                    self.itemsArray?.append(itemModel)
                }
                
                // then save _itemsArray -> Coredata
                self.savetoCoredata(itemModelArray: self.itemsArray!)
                
                self.loadingActivityIndicator.stopAnimating()
                
                self.performSegue(withIdentifier: Constants.itemsVC_Segue_CollectionViewController, sender: self)
                self.performSegue(withIdentifier: Constants.itemsVC_Segue_TableviewController, sender: self)
            }else{
                
                self.loadingActivityIndicator.stopAnimating()
                
                // failure
                let alertController:UIAlertController = UIAlertController.init(title: "Error", message: message as? String, preferredStyle: .alert)
                let okAction:UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }

    }
    
    func savetoCoredata(itemModelArray: [ItemModel]){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        appDelegate.dataController.save(arrayOfDic: self.itemsArray!)
    }
    
    func getfromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        appDelegate.dataController.fetchItems { (isSuccess:Bool, arrayofItemsModel:[ItemModel]?) in
            if (isSuccess == true){
                self.itemsArray = arrayofItemsModel
                self.performSegue(withIdentifier: Constants.itemsVC_Segue_TableviewController, sender: self)
            }else{
                print("Error on retrieving core Data")
            }
        }
        
    }
    
    //MARK: Reachabilty Scenario
    
    func reachabiltyScenario() {
        if Reachability.isInternetAvailable() == true {
            // Internet
            // init by Grid contasinerView
            self.listGridBarButtonItem.image = UIImage(named: "Grid")
            self.listGridBarButtonItem.tintColor = UIColor.white
            
            // fill ItemsArray
            self.fillItemsArray()
        }
        else{
            
            //============= offline =============================
            //1- disable List/Grid button
            self.listGridBarButtonItem.isEnabled = false
            self.listGridBarButtonItem.image = UIImage(named: "List")
            
            //2- Hide CoolectionContainerView
            self.collectionViewContainerView.isHidden = true
            
            //3- fill items by CoreData
            self.getfromCoreData()
            
        }
    }
    
    //MARK:- BarButtonAction
    
    @IBAction func listGridBarButtonPressed(_ sender: UIBarButtonItem) {
        if doubleTap == true {
            //Second Tap
            doubleTap = false
            UIView.animate(withDuration: 0.5, animations: {
                self.listGridBarButtonItem.image = UIImage(named: "Grid")
                self.collectionViewContainerView.alpha = 1
                self.tableViewContainerView.alpha = 0
            })
        } else {
            //First Tap
            doubleTap = true
            UIView.animate(withDuration: 0.5, animations: {
                self.listGridBarButtonItem.image = UIImage(named: "List")
                self.tableViewContainerView.alpha = 1
                self.collectionViewContainerView.alpha = 0
            })
        }
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.itemsVC_Segue_CollectionViewController {
            let itemsCollectionVC = segue.destination as! ItemsCollectionViewController
            itemsCollectionVC.itemsCollectionViewArray = self.itemsArray
        }
        if segue.identifier == Constants.itemsVC_Segue_TableviewController {
            let itemsTableVC = segue.destination as! ItemsTableViewController
            itemsTableVC.itemsTableViewArray = self.itemsArray
            
        }
     }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any!) -> Bool {
        if let itemsCount: Int = self.itemsArray?.count, itemsCount > 0{
            return true
        }
        return false
    }
    
}
