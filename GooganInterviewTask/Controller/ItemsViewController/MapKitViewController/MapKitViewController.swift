//
//  MapKitViewController.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/30/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit
import MapKit

class MapKitViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var map: MKMapView!
    var locationManager: CLLocationManager!
    
    let currentLocationPin = CustomPointAnnotation()
    var currentPinAnnotationView:MKPinAnnotationView!
    
    let sourcePin = CustomPointAnnotation()
    var sourcePinAnnotationView:MKPinAnnotationView!
    
    let destinationPin = CustomPointAnnotation()
    var destinationPinAnnotationView:MKPinAnnotationView!
    
    var itemModel: ItemModel!
    
    var index:Bool = false
    
    var routeColor: String? = "Green"
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var startFinishButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        // source && destination pins
        sourcePin.title = "Source"
        sourcePin.coordinate = CLLocationCoordinate2D(latitude: Double(self.itemModel.fromLat)!, longitude: Double(itemModel.fromLong)!)
        sourcePinAnnotationView = MKPinAnnotationView(annotation: sourcePin, reuseIdentifier: "sourcePin")
        map.addAnnotation(sourcePinAnnotationView.annotation!)
        
        destinationPin.title = "Destination"
        destinationPin.coordinate = CLLocationCoordinate2D(latitude: Double(itemModel.toLat)!, longitude: Double(itemModel.toLong)!)
        destinationPinAnnotationView = MKPinAnnotationView(annotation: destinationPin, reuseIdentifier: "destinationPin")
        map.addAnnotation(destinationPinAnnotationView.annotation!)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- Fit All Pins
    
    func fitAllPins() {
        var zoomRect = MKMapRectNull;
        let myLocationPointRect = MKMapRectMake(currentLocationPin.coordinate.longitude, currentLocationPin.coordinate.latitude, 0, 0)
        let currentDestinationPointRect = MKMapRectMake(destinationPin.coordinate.longitude, destinationPin.coordinate.latitude, 0, 0)
        
        zoomRect = myLocationPointRect;
        zoomRect = MKMapRectUnion(zoomRect, currentDestinationPointRect);
        
        map.setVisibleMapRect(zoomRect, animated: true)
    }
    
    //MARK:- RouteOnMap
    
    func showRouteOnMap(annotation: MKPointAnnotation) {
        
        // clear timeLabel
        self.timeLabel.text = ""
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: currentLocationPin.coordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: annotation.coordinate, addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else {
                UIApplication.shared.endIgnoringInteractionEvents()
                let alertCont: UIAlertController = UIAlertController.init(title: "Far from", message: "Too Long distance to settle here", preferredStyle: .alert)
                let okAction: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                alertCont.addAction(okAction)
                self.present(alertCont, animated: true, completion: nil)
                return
            }
            
            if (unwrappedResponse.routes.count > 0) {
                if let routeResponse = response?.routes {
                    let quickestRouteForSegment: MKRoute =
                        routeResponse.sorted(by: {$0.expectedTravelTime <
                            $1.expectedTravelTime})[0]
                    
                    var timeVar = TimeInterval()
                    timeVar += quickestRouteForSegment.expectedTravelTime
                    // print time -> timeLabel
                    self.printTime(time: timeVar)
                    
                    // set root color
                    if let annotationTitle = annotation.title, annotationTitle == "Source"{
                        self.routeColor = "Green"
                    }else{
                        self.routeColor = "Red"
                    }
                    // add polyline
                    self.map.add(unwrappedResponse.routes[0].polyline)
                    // fit pins "polyline"
                    self.map.setVisibleMapRect(unwrappedResponse.routes[0].polyline.boundingMapRect, animated: true)
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    
    func printTime(time: TimeInterval){
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        self.timeLabel.text = ("\(hours):\(minutes):\(seconds)")
    }
    
    //MARK:- Start/Finish button action
    
    @IBAction func startFinishButtonPressed(_ sender: UIButton) {
        // clear old overlay
        self.map.removeOverlays(self.map.overlays as [MKOverlay])
        if index == true {
            self.startFinishButton.setTitle("Start", for: .normal)
            self.showRouteOnMap(annotation: sourcePin)
            index = false
        }
        else {
            self.startFinishButton.setTitle("Finish", for: .normal)
            self.showRouteOnMap(annotation: destinationPin)
            index = true
        }
    }
    
    //MARK:- CLLocationManager delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        //let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        //let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        //self.map.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        currentLocationPin.coordinate = (location?.coordinate)!
        currentLocationPin.title = "Current Location"
        currentLocationPin.subtitle = "You are here!"
        currentPinAnnotationView = MKPinAnnotationView(annotation: currentLocationPin, reuseIdentifier: "currentPin")
        map.addAnnotation(currentPinAnnotationView.annotation!)
        
        // stop mapkit location updating
        locationManager.stopUpdatingLocation()
        
        // fit all AnnotationPin -> MapKit visibale
        self.map.showAnnotations(self.map.annotations, animated: true)
        
        // first -> show route between current && source
        self.showRouteOnMap(annotation: sourcePin)
    }
    
    //MARK: - Custom Annotation
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "pin"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            
            if let title = annotation.title {
                switch title! {
                case "Current Location":
                    annotationView.image = UIImage(named: "CurrentPin")
                    break
                case "Source":
                    annotationView.image = UIImage(named: "SourcePin")
                    break
                case "Destination":
                    annotationView.image = UIImage(named: "DestinationPin")
                    break
                default:
                    break
                }
            }
            
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polyLine = overlay
        let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
        
        if let color:String = self.routeColor, color == "Green"{
            polyLineRenderer.strokeColor = UIColor.green
        }
        else{
            polyLineRenderer.strokeColor = UIColor.red
        }
        polyLineRenderer.lineWidth = 2.0
        
        return polyLineRenderer
    }
    
}

