//
//  ItemsTableViewController.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/29/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class ItemsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    var itemsTableViewArray: [ItemModel]! {
        didSet{
            self.itemsTableView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.itemsTableView.rowHeight = UITableViewAutomaticDimension
        self.itemsTableView.estimatedRowHeight = 100
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.itemsTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == Constants.mapKitSegue{
            let indexPath: IndexPath = self.itemsTableView.indexPathForSelectedRow!
            let itemModel: ItemModel = self.itemsTableViewArray[indexPath.row]
            let mapKitVC: MapKitViewController = segue.destination as! MapKitViewController
            mapKitVC.itemModel = itemModel
            
        }
    }
 
    
    //MARK:- UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsTableViewArray.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ItemsTableViewCell = self.itemsTableView.dequeueReusableCell(withIdentifier: Constants.itemCellIdentifier, for: indexPath) as! ItemsTableViewCell
        
        // configure Cell
        let itemModel: ItemModel = self.itemsTableViewArray[indexPath.row]
        cell.itemFromLabel?.text = "From: \(itemModel.fromAddress as String)"
        cell.itemToLabel?.text = "To: \(itemModel.toAddress as String)"
        cell.imageView?.image = UIImage(named: "Place")
        
        // fit LabelText width
        cell.itemFromLabel?.adjustsFontSizeToFitWidth = true
        cell.itemFromLabel?.numberOfLines = 0
        cell.itemToLabel?.adjustsFontSizeToFitWidth = true
        cell.itemToLabel?.numberOfLines = 0
        
        return cell
    }
    
    //MARK:- UITableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // tableView dynamice cell hieght
        return UITableViewAutomaticDimension
    }
    
    

}
