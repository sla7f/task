//
//  ViewController.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loadingSuperContainerView: UIView!
    @IBOutlet weak var loadingContainerView: UIView!
    @IBOutlet weak var usernameTextFiled: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var testButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // disable TestButton
        self.testButton.isEnabled = false
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // textFields observer
        self.usernameTextFiled.addTarget(self, action: #selector(LoginViewController.checkFields(_:)), for: .editingDidEnd)
        self.passwordTextField.addTarget(self, action: #selector(LoginViewController.checkFields(_:)), for: .editingDidEnd)
        
        // layout
        self.usernameTextFiled.layer.borderColor = UIColor(hexString: Constants.App_Global_Color).cgColor
        self.usernameTextFiled.layer.borderWidth = 1.0
        self.passwordTextField.layer.borderColor = UIColor(hexString: Constants.App_Global_Color).cgColor
        self.passwordTextField.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK- TextField Observer
    
    func checkFields(_ sender: UITextField) {
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        guard
            let userName = self.usernameTextFiled.text, !userName.isEmpty,
            let password = self.passwordTextField.text, !password.isEmpty
            else { return }
        // enable testbutton in case of match pervious conditions
        self.testButton.isEnabled = true
    }


    @IBAction func testButtonPressed(_ sender: UIButton) {
        self.addLoadingSuperView()
        let testApi: TestAPI = TestAPI(userName: self.usernameTextFiled.text!, password: self.passwordTextField.text!)
        testApi.testAPIByUsernamePassword { (status, message) in
            self.removeLoadingSuperView()
            if status == "success"{
                // success
                self.performSegue(withIdentifier: Constants.itemsSegue, sender: self)
            }else{
                // failure
                let failureAlert: UIAlertController = UIAlertController.init(title: "Login Failure", message: message, preferredStyle: .alert)
                let okAction:UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
                failureAlert.addAction(okAction)
                self.present(failureAlert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.itemsSegue {
            let navigationController: UINavigationController = segue.destination as! UINavigationController
            let _: ItemsViewController = navigationController.topViewController as! ItemsViewController
        }
    }
    
    //MARK:- LoadingScenario
    
    func addLoadingSuperView() {
        self.loadingSuperContainerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.loadingSuperContainerView)
    }
    
    func removeLoadingSuperView() {
        self.loadingSuperContainerView.removeFromSuperview()
    }
    
    //MARK:- Dismiss Keyboard
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

