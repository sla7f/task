//
//  Constants.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/26/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import Foundation

class Constants {
    
    // itemsSegue
    static let itemsSegue: String = "ItemsSegue"
    
    //==================== ItemsViewController ======================//
    static let itemsVC_Segue_CollectionViewController: String = "CollectionViewSegue"
    static let itemsVC_Segue_TableviewController: String = "TableViewSegue"
    //==============================================================//
    
    //==================== TableView ================================//
    static let mapKitSegue = "MapSegue"
    //================================================================//

    static let itemCellIdentifier: String = "ItemCell"
    
    //==================== App Color =================================//
    static let App_Global_Color: String = "FCCB53"
    //===============================================================//
}
