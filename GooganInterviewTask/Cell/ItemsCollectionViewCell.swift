//
//  ItemsCollectionViewCell.swift
//  GooganInterviewTask
//
//  Created by Mina on 12/29/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class ItemsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemFromAddressLabel: UILabel!
    @IBOutlet weak var itemToAddressLabel: UILabel!
}
