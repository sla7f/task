//
//  ItemsTableViewCell.swift
//  GooganInterviewTask
//
//  Created by MACBOOK AIR on 12/31/16.
//  Copyright © 2016 myOrganization. All rights reserved.
//

import UIKit

class ItemsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemFromLabel: UILabel!
    @IBOutlet weak var itemToLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
